package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"online_market/online_market_go_user_service/genproto/user_service"
	"online_market/online_market_go_user_service/models"
	"online_market/online_market_go_user_service/pkg/helper"
	"online_market/online_market_go_user_service/storage"
)

type UserRepo struct {
	db *pgxpool.Pool
}

func NewUserRepo(db *pgxpool.Pool) storage.UserRepoI {
	return &UserRepo{
		db: db,
	}
}

func (c *UserRepo) Create(ctx context.Context, req *user_service.CreateUser) (resp *user_service.UserPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "user" (
				id,
				first_name,
				last_name,
				phone_number,
				date_of_birth,
				status,
				balance,
				nickname,
				password,
				updated_at
			) VALUES ( $1, $2, $3, $4, $5, $6, $7, $8, $9, now() )
		`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.FirstName,
		req.LastName,
		req.PhoneNumber,
		req.DateOfBirth,
		req.Status,
		req.Balance,
		req.Nickname,
		req.Password,
	)

	if err != nil {
		return nil, err
	}

	return &user_service.UserPrimaryKey{Id: id.String()}, nil
}

func (c *UserRepo) GetByPKey(ctx context.Context, req *user_service.UserPrimaryKey) (resp *user_service.User, err error) {

	query := `
		SELECT
			id,
			first_name,
			last_name,
			phone_number,
			date_of_birth,
			status,
			cast(balance AS VARCHAR),
			nickname,
			password,
			created_at,
			updated_at
		FROM "user"
		WHERE deleted_at IS NULL AND id = $1
	`

	var (
		id            sql.NullString
		first_name    sql.NullString
		last_name     sql.NullString
		phone_number  sql.NullString
		date_of_birth sql.NullString
		status        sql.NullString
		balance       sql.NullFloat64
		nickname      sql.NullString
		password      sql.NullString
		createdAt     sql.NullString
		updatedAt     sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&first_name,
		&last_name,
		&phone_number,
		&date_of_birth,
		&status,
		&balance,
		&nickname,
		&password,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &user_service.User{
		Id:          id.String,
		FirstName:   first_name.String,
		LastName:    last_name.String,
		PhoneNumber: phone_number.String,
		DateOfBirth: date_of_birth.String,
		Status:      status.String,
		Balance:     balance.Float64,
		Nickname:    nickname.String,
		Password:    password.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *UserRepo) GetUserData(ctx context.Context, req *user_service.UserAuthorize) (resp *user_service.User, err error) {
	query := `
		SELECT
			id,
			first_name,
			last_name,
			phone_number,
			date_of_birth,
			status,
			cast(balance AS VARCHAR),
			nickname,
			password,
			created_at,
			updated_at
		FROM "user"
		WHERE deleted_at IS NULL AND nickname = $1 AND password = $2
	`

	var (
		id            sql.NullString
		first_name    sql.NullString
		last_name     sql.NullString
		phone_number  sql.NullString
		date_of_birth sql.NullString
		status        sql.NullString
		balance       sql.NullFloat64
		nickname      sql.NullString
		password      sql.NullString
		createdAt     sql.NullString
		updatedAt     sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Nickname, req.Password).Scan(
		&id,
		&first_name,
		&last_name,
		&phone_number,
		&date_of_birth,
		&status,
		&balance,
		&nickname,
		&password,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &user_service.User{
		Id:          id.String,
		FirstName:   first_name.String,
		LastName:    last_name.String,
		PhoneNumber: phone_number.String,
		DateOfBirth: date_of_birth.String,
		Status:      status.String,
		Balance:     balance.Float64,
		Nickname:    nickname.String,
		Password:    password.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *UserRepo) GetAll(ctx context.Context, req *user_service.GetListUserRequest) (resp *user_service.GetListUserResponse, err error) {

	resp = &user_service.GetListUserResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		// filter = " WHERE TRUE"
		sort = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			first_name,
			last_name,
			phone_number,
			date_of_birth,
			status,
			cast(balance AS VARCHAR),
			nickname,
			password,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "user"
		WHERE deleted_at IS NULL
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id            sql.NullString
			first_name    sql.NullString
			last_name     sql.NullString
			phone_number  sql.NullString
			date_of_birth sql.NullString
			status        sql.NullString
			balance       sql.NullFloat64
			nickname      sql.NullString
			password      sql.NullString
			createdAt     sql.NullString
			updatedAt     sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&first_name,
			&last_name,
			&phone_number,
			&date_of_birth,
			&status,
			&balance,
			&nickname,
			&password,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Users = append(resp.Users, &user_service.User{
			Id:          id.String,
			FirstName:   first_name.String,
			LastName:    last_name.String,
			PhoneNumber: phone_number.String,
			DateOfBirth: date_of_birth.String,
			Status:      status.String,
			Balance:     balance.Float64,
			Nickname:    nickname.String,
			Password:    password.String,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}

	return resp, nil
}

func (c *UserRepo) Update(ctx context.Context, req *user_service.UpdateUser) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "user"
			SET
				first_name = :first_name,
				last_name = :last_name,
				phone_number = :phone_number,
				date_of_birth = :date_of_birth,
				status = :status,
				balance = :balance,
				nickname = :nickname,
				password = :password,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":            req.GetId(),
		"first_name":    req.GetFirstName(),
		"last_name":     req.GetLastName(),
		"phone_number":  req.GetPhoneNumber(),
		"date_of_birth": req.GetDateOfBirth(),
		"status":        req.GetStatus(),
		"balance":       req.GetBalance(),
		"nickname":      req.GetNickname(),
		"password":      req.GetPassword(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *UserRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"user"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *UserRepo) Transaction(ctx context.Context, req *user_service.UserTransaction) error {

	query := `UPDATE "user" SET balance = $2 WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.UserId, req.AmountOfMoney)

	if err != nil {
		return err
	}

	return nil
}

func (c *UserRepo) Delete(ctx context.Context, req *user_service.UserPrimaryKey) error {

	query := `UPDATE "user" SET deleted_at = now() WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
