
CREATE TABLE  IF NOT EXISTS "user"(
    id UUID PRIMARY KEY NOT NULL,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    phone_number VARCHAR(25) NOT NULL,
    date_of_birth VARCHAR(10) NOT NULL,
    status VARCHAR(9) NOT NULL,
    balance NUMERIC NOT NULL,
    nickname VARCHAR(200) UNIQUE NOT NULL,
    password VARCHAR(100) NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);