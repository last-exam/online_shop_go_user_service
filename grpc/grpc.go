package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"online_market/online_market_go_user_service/config"
	"online_market/online_market_go_user_service/genproto/user_service"
	"online_market/online_market_go_user_service/grpc/client"
	"online_market/online_market_go_user_service/grpc/service"
	"online_market/online_market_go_user_service/pkg/logger"
	"online_market/online_market_go_user_service/storage"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	user_service.RegisterUserServiceServer(grpcServer, service.NewUserService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
